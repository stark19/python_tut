a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]

for element in a:
    if int(element) < 5:
        print(element)

newList = [ e for e in a if int(e) < 5 ]
print(newList)