import random

def gen_num():
    return random.randint(1000,9999)

def check_number(number,right_number):
    cows = 0
    bulls = 0
    if number == right_number:
        print("Correct my friend")
        global game_end
        game_end = True
        return

    number_lista = [int(i) for i in str(number)]
    right_number_lista = [int(i) for i in str(right_number)]

    for j in range(len(number_lista)):
        if (number_lista[j] == right_number_lista[j]):
            cows += 1
        for k in range(len(right_number_lista)):
            if(right_number_lista[k] == number_lista[j] and right_number_lista[j] != number_lista[j]):
                bulls += 1

    print("Your number: " + str(number) + " Cow count: " + str(cows)  + " Bull count: " + str(bulls))
    print ("Correct number: " + str(right_number))

tries = 0
game_end = False
user_input = 0
correct_number = gen_num()
while not game_end:
    user_input = input("Guess 4 digit number ")
    check_number(int(user_input),correct_number)
    tries += 1
print("Yo man you did it. Number of tries: " + str(tries))