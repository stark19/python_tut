dict = {}
with open('file_to_read2.txt', 'r') as open_file:
    line = open_file.readline()
    table = line.split('/')
    current = table[2]
    prev = table[2]
    while line:
        table = line.split('/')
        prev = current
        current = table[2]
        if current not in dict.keys():
            dict[current] = 1
        else:
            dict[current] = dict[current] + 1
        line = open_file.readline()

print(dict)
print(dict['operating_room'])