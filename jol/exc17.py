import requests
from bs4 import BeautifulSoup

#Code no longer works because nytimes uses dynamic webistes now

url = "http://www.nytimes.com"
r = requests.get(url)

soup = BeautifulSoup(r.content, "html.parser")
title_elems = soup.find_all("span",  class_="balancedHeadline")

for title_elem in title_elems:
    print(title_elem.text)
